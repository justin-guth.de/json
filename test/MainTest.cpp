#include "catch_test_macros.hpp"
#include "json.h"

using namespace JG::json;

SCENARIO("A json instance can be created", "[Json]")
{
    GIVEN("No parameters")
    {
        WHEN("The object is created")
        {
            Json json;

            THEN("The initial type is null")
            {
                REQUIRE(json.getType() == JsonType::Null);
            }
        }
    }
}