namespace JG
{
	namespace json
	{
		enum class JsonType
		{

			Null = 0,
			Number,
			String
		};

		template<typename T = int32_t>
		class TemplatedJson
		{

		public:

			inline JsonType getType() { return this->type; }

			template<typename Type> Type value();
			TemplatedJson<T>& operator=(int64_t number);
			TemplatedJson<T>& operator=(double number);

		private:

			JsonType type = JsonType::Null;

			union dataPointer
			{
				double* number = nullptr;
				char* string;
			};
		};

		typedef TemplatedJson<> Json;
	} // namespace json
} // namespace JG

template<typename T>
template<typename Type>
inline Type JG::json::TemplatedJson<T>::value()
{
	switch (this->type) {

	case JsonType::Null:
		return;

	}
}

template<typename T>
inline JG::json::TemplatedJson<T>& JG::json::TemplatedJson<T>::operator=(int64_t number) {

	this->type = JG::json::JsonType::Number;
}

template<typename T>
inline JG::json::TemplatedJson<T>& JG::json::TemplatedJson<T>::operator=(double number) {

	this->type = JG::json::JsonType::Number;
}